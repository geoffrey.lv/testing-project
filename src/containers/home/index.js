import React from 'react'
import { connect } from 'react-redux'

import withImmutable from '../../components/withImmutableData';

const Home = props => {
  console.log('HOME RENDERED');
  return (
      <div>
        <h1>Home</h1>
        <p>Count: {props.count.num}</p>

        <p>
          <button onClick={props.increment}>Increment</button>
          <button onClick={props.doSomethingElse}>Something Else</button>
        </p>
      </div>
  );
};

const mapStateToProps = state => {
  return {
    count: state.counter.get('value'),
  };
};

const mapDispatchToProps = dispatch => ({
  increment: () => dispatch({type :  'INCREMENT'}),
  doSomethingElse: () => dispatch({type : 'DO_SOMETHING_ELSE'}),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withImmutable(Home))
