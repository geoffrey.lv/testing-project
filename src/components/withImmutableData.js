export default function(component) {
  return (props) => {
    const convertedProps = Object.keys(props).reduce((acc, propKey) => {
      if (!props[propKey]) {
        return acc;
      }

      if (props[propKey].toJS) {
        acc[propKey] = props[propKey].toJS();
        return acc;
      }

      acc[propKey] = props[propKey];
      return acc;
    }, {});

    return component(convertedProps);
  };
}