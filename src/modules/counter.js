import {fromJS} from 'immutable';

const initialState = fromJS({
  value: {
    num: 0
  },
  other : null
});

export default function counter(state = initialState, action) {
  console.log(action.type);
  switch (action.type) {
    case 'INCREMENT':
      const value = state.getIn(['value', 'num']);
      return state.setIn(['value', 'num'], value + 1);
    case 'DO_SOMETHING_ELSE':
      return state.set('other', Math.random() * 100);
    default:
      return state;
  };
}